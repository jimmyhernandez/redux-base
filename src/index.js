import C from './constants';
import appReducer from './store/reducers';
import initialState from './initialState.json';
import {createStore} from 'redux';

const store = createStore(appReducer,initialState);

console.log('initial state', store.getState());

store.dispatch({
  type: C.ADD_DAY,
  payload: {
    "resort": "Squaw Valley",
    "date": "2018-09-28",
    "powder": false,
    "backcountry": true
  }
});

console.log('initial state', store.getState());

store.dispatch({
  type: C.SET_GOAL,
  payload: 123
});

console.log('initial state', store.getState());

// let state = initialState;

// console.log(`
//   Initial State
//   ==============================================
//   Goal: ${state.goal}
//   Resorts: ${JSON.stringify(state.allSkiDays)}
//   Fetching: ${state.resortNames.fetching}
//   Suggestions: ${state.resortNames.suggestions}
// `);

// state = appReducer(state,{
//   type: C.SET_GOAL,
//   payload: 2
// });

// state = appReducer(state,{
//   type: C.ADD_DAY,
//   payload: {
//     "resort": "Mont Hilaire",
//     "date": "2018-10-26",
//     "powder": true,
//     "backcountry": false
//   }
// });

// state = appReducer(state,{
//   type: C.CHANGE_SUGGESTIONS,
//   payload: ["Mt Tallac", "Mt Hood", "Mt Shsaste"]
// })

// console.log(`
//   Next State
//   ==============================================
//   Goal: ${state.goal}
//   Resorts: ${JSON.stringify(state.allSkiDays)}
//   Fetching: ${state.resortNames.fetching}
//   Suggestions: ${state.resortNames.suggestions}
// `);


// import C from './constants';
// import {goal, skiDay, errors, allSkiDays, fetching, suggestions} from './store/reducers';

// // const state = 10;

// // const action = {
// //   type: C.SET_GOAL,
// //   payload: 15
// // }

// // const nextState = goal(state,action);

// // console.log(`
// //   initial goal: ${state}
// //   action: ${JSON.stringify(action)}
// //   new state: ${nextState}
// // `);

// // const state = null;

// // const action = {
// //   type: C.ADD_DAY,
// //   payload: {
// //     "resort": "Heavenly",
// //     "date": "2018-10-25",
// //     "powder": true,
// //     "backcountry": false
// //   }
// // }

// // const nextState = skiDay(state,action);

// // console.log(`
// //   initial goal: ${state}
// //   action: ${JSON.stringify(action)}
// //   new state: ${JSON.stringify(nextState)}
// // `);

// // const state = [
// //   "user not authorized",
// //   "server feed not found"
// // ];

// // const action = {
// //   type: C.ADD_DAY,
// //   payload: "cannot connect to server"
// // }

// // const nextState = errors(state,action);

// // console.log(`
// //   initial goal: ${state}
// //   action: ${JSON.stringify(action)}
// //   new state: ${JSON.stringify(nextState)}
// // `);

// // const state = [
// //   "user not authorized",
// //   "server feed not found"
// // ];

// // const action = {
// //   type: C.CLEAR_ERROR,
// //   payload: 0
// // }

// // const nextState = errors(state,action);

// // console.log(`
// //   initial goal: ${state}
// //   action: ${JSON.stringify(action)}
// //   new state: ${JSON.stringify(nextState)}
// // `);

// // const state = [
// //   {
// //   "resort": "Kirkwood",
// //   "date": "2018-10-25",
// //   "powder": true,
// //   "backcountry": false
// //   },
// //   {
// //     "resort": "Boreal",
// //     "date": "2018-10-26",
// //     "powder": true,
// //     "backcountry": true 
// //   }
// // ];

// // const action = {
// //   type: C.REMOVE_DAY,
// //   payload: "2018-10-25" 
// // }

// // const nextState = allSkiDays(state,action);

// // console.log(`
// //   initial goal: ${JSON.stringify(state)}
// //   action: ${JSON.stringify(action)}
// //   new state: ${JSON.stringify(nextState)}
// // `);

// const action = {
//   type: C.CHANGE_SUGGESTIONS,
//   payload: ["Heavenly Ski Resort","Heavens Sonohara"]
// }

// const state = {
//   fetching: true,
//   suggestions: []
// }

// const expectedState = {
//   fetching: false,
//   suggestions: ["Heavenly Ski Resort","Heavens Sonohara"] 
// }

// const actualState = {
//   fetching: fetching(state.fetching,action),
//   suggestions: suggestions(state.suggestions, action)
// }

