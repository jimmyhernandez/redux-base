import C from './constants';
import appReducer from './store/reducers';
import initialState from './initialState.json';

let state = initialState;

console.log(`
  Initial State
  ==============================================
  Goal: ${state.goal}
  Resorts: ${JSON.stringify(state.allSkiDays)}
  Fetching: ${state.resortNames.fetching}
  Suggestions: ${state.resortNames.suggestions}
`);

state = appReducer(state,{
  type: C.SET_GOAL,
  payload: 2
});

state = appReducer(state,{
  type: C.ADD_DAY,
  payload: {
    "resort": "Mont Hilaire",
    "date": "2018-10-26",
    "powder": true,
    "backcountry": false
  }
});

state = appReducer(state,{
  type: C.CHANGE_SUGGESTIONS,
  payload: ["Mt Tallac", "Mt Hood", "Mt Shsaste"]
})

console.log(`
  Next State
  ==============================================
  Goal: ${state.goal}
  Resorts: ${JSON.stringify(state.allSkiDays)}
  Fetching: ${state.resortNames.fetching}
  Suggestions: ${state.resortNames.suggestions}
`);